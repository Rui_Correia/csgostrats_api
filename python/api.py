import json,sqlite3,hashlib, binascii, os,random,string
from flask import Flask
from flask import request
import datetime

def checkIfUserIsPremium(id):
    response = False
    conn = sqlite3.connect('csgo_strats.db')
    c = conn.cursor()
    c.execute("SELECT status from users where id = ?",(id,))
    result = c.fetchall()
    for res in result:
        if res[0] == 'premium':
            response = True
        else:
            response = False

    return response

def isValidToken(token):
    if token == 'debug':
        return True

    response = True
    conn = sqlite3.connect('csgo_strats.db')
    c = conn.cursor()
    c.execute("SELECT valid_to from users where token = ?",(token,))
    result = c.fetchall()
    for res in result:
        date_db = datetime.datetime.strptime(res[0],'%Y-%m-%d')
        now = datetime.date.today()
        if now > date_db:
            response = False

    return response


def randomString(stringLength=12):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


def hash_password(password):
    """Hash a password for storing."""
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),
                                salt, 100000)
    pwdhash = binascii.hexlify(pwdhash)
    return (salt + pwdhash).decode('ascii')


def verify_password(stored_password, provided_password):
    """Verify a stored password against one provided by user"""
    salt = stored_password[:64]
    stored_password = stored_password[64:]
    pwdhash = hashlib.pbkdf2_hmac('sha512',
                                  provided_password.encode('utf-8'),
                                  salt.encode('ascii'),
                                  100000)
    pwdhash = binascii.hexlify(pwdhash).decode('ascii')
    return pwdhash == stored_password


app = Flask(__name__)
app.config["DEBUG"] = True


@app.route('/utility/<string:func_name>', methods=['POST'])
def utility_func(func_name):
    response = {'status': 200,'message':'','data':{}}
    if func_name == 'all':
        data = request.data
        if data == '' or data is None:
            token = request.form.get('token')
            map_id = request.form.get('map_id')
            author_id = request.form.get('author_id')
        else:
            parsed_data = json.loads(data)
            token = parsed_data["token"]
            map_id = parsed_data["map_id"]
            author_id = parsed_data["author_id"]

        if token is None or isValidToken(token) is False:
            response['status'] = 400
            response['message'] = 'Token Expired'
        else:
            conn = sqlite3.connect('csgo_strats.db')
            c = conn.cursor()
            c.execute("SELECT id,type,location,url FROM utility WHERE map_id = ? and (author_id = 'default' or author_id = 'public' or author_id = ? ",(map_id,author_id))
            result = c.fetchall()
            obj = []
            for res in result:
                data_2 = {"id": res[0], "type": res[1], "location": res[2],"url":res[3]}
                obj.append(data_2)

            response['data'] = obj

    elif func_name == 'new':
        data = request.data
        if data == '' or data is None:
            token = request.form.get('token')
            map_id = request.form.get('map_id')
            location = request.form.get('location')
            url = request.form.get('url')
            type = request.form.get('type')
            author_id = request.form.get('author_id')

        else:
            parsed_data = json.loads(data)
            token = parsed_data["token"]
            map_id = parsed_data["map_id"]
            location = parsed_data["location"]
            url = parsed_data["url"]
            type = parsed_data["type"]
            author_id = parsed_data["author_id"]

        if token is None or isValidToken(token) is False:
            response['status'] = 400
            response['message'] = 'Token Expired'

        if checkIfUserIsPremium(author_id) is False:
            author_id = 'public'

        conn = sqlite3.connect('csgo_strats.db')
        c = conn.cursor()
        c.execute("INSERT INTO utility values(null,?,?,?,?,?",(type,location,url,map_id,author_id,))
        conn.commit()
        conn.close()
        response['message'] = 'success'
    else:
        response['status'] = 400
        response['message'] = 'Method not Implemented'

    return json.dumps(response)


#
# @app.route('/register', methods=['POST'])
# def register():
#     conn = sqlite3.connect('csgo_strats.db')
#     c = conn.cursor()
#     email = request.form.get('email')
#     password = request.form.get('password')
#     name = request.form.get('name')
#     hashed_password = hash_password(password)
#     c.execute("UPDATE users set password = ? WHERE name = ?", (hashed_password,name,))
#     conn.commit()
#     conn.close()
#     return 'C'


@app.route('/strats/', methods=['POST'])
def getStrats():
    response = {'status': 200,'message':'','data':{}}
    data = request.data  # data from mobile must be parsed
    if data == '' or data is None:
        token = request.form.get('token')
        map_id = request.form.get('map_id')
       # author_id = request.form.get('author_id')

    else:
        parsed_data = json.loads(data)
        token = parsed_data["token"]
        map_id = parsed_data["map_id"]
        # author_id = parsed_data["author_id"]

    if token is None or isValidToken(token) is False:
        response['status'] = 400
        response['message'] = 'Token Expired'

    else:
        response['message'] = 'success'
        conn = sqlite3.connect('csgo_strats.db')
        c = conn.cursor()
        c.execute("SELECT id,side,name,player_1,player_2,player_3,player_4,player_5 FROM strats WHERE author_id = ?  ",('default',))
        result = c.fetchall()
        obj = []

        for res in result:
            data = {"id": res[0], "name": res[2], "side": res[1],"player_1" : res[3], "player_2": res[4], "player_3": res[5], "player_4" : res[6], "player_5": res[7]}

            players_utility_1 = []
            players_utility_2 = []
            players_utility_3 = []
            players_utility_4 = []
            players_utility_5 = []

            if data['player_1'] != '':

                c.execute("SELECT type,location,url from utility WHERE id in ("+data['player_1']+") and map_id = ? ",(map_id, ))
                result2 = c.fetchall()
                for res2 in result2:
                    data_2 = {"type": res2[0],"location": res2[1] , "url" : res2[2]}
                    players_utility_1.append(data_2)

            if data['player_2'] != '' and data['player_2'] is not None:
                c.execute("SELECT type,location,url from utility WHERE id in ("+data['player_2']+")  and map_id = ? ",(map_id, ))
                result2 = c.fetchall()
                for res2 in result2:
                    data_2 = {"type": res2[0],"location": res2[1] , "url" : res2[2]}
                    players_utility_2.append(data_2)

            if data['player_3'] != '' and data['player_3'] is not None:
                c.execute("SELECT type,location,url from utility WHERE id in ("+data['player_3']+")  and map_id = ? ",(map_id, ))
                result2 = c.fetchall()
                for res2 in result2:
                    data_2 = {"type": res2[0],"location": res2[1] , "url" : res2[2]}
                    players_utility_3.append(data_2)

            if data['player_4'] != '' and data['player_4'] is not None:
                c.execute("SELECT type,location,url from utility WHERE id in ("+data['player_4']+")  and map_id = ? ",(map_id, ))
                result2 = c.fetchall()
                for res2 in result2:
                    data_2 = {"type": res2[0],"location": res2[1] , "url" : res2[2]}
                    players_utility_4.append(data_2)

            if data['player_5'] != '' and data['player_5'] is not None:
                c.execute("SELECT type,location,url from utility WHERE id in ("+data['player_5']+")  and map_id = ? ",(map_id, ))
                result2 = c.fetchall()
                for res2 in result2:
                    data_2 = {"type": res2[0],"location": res2[1] , "url" : res2[2]}
                    players_utility_5.append(data_2)

            data['player_1'] = players_utility_1
            data['player_2'] = players_utility_2
            data['player_3'] = players_utility_3
            data['player_4'] = players_utility_4
            data['player_5'] = players_utility_5

            obj.append(data)

        response['data'] = obj

    return json.dumps(response)


@app.route('/maps', methods=['POST'])
def getMaps():
    response = {'status': 200,'message':'','data':{}}
    data = request.data  # data from mobile must be parsed
    if data == '' or data is None:
        token = request.form.get('token')
    else:
        parsed_data = json.loads(data)
        token = parsed_data["token"]

    if token is None or isValidToken(token) == False:
        response['status'] = 400
        response['message'] = 'Token Expired'

    else:
        response['message'] ='success'
        conn = sqlite3.connect('csgo_strats.db')
        c = conn.cursor()
        c.execute("SELECT id,name,type FROM maps ")
        result = c.fetchall()
        obj = []

        for res in result:
            data = {"id": res[0], "name": res[1], "type": res[2]}
            c.execute("SELECT id,type,location,url FROM utility WHERE map_id = ?",(res[0],))
            result2 = c.fetchall()
            utility = []
            for res2 in result2:
                data_2 = {"id": res2[0], "type": res2[1], "location": res2[2],"url":res2[3]}
                utility.append(data_2)

            data['utility'] = utility
            obj.append(data)

        response['data']= obj

    return json.dumps(response)


@app.route('/login', methods=['POST'])
def login():
    response = {'status': 400,'message':'Login Failed','data':{}}
    data = request.data  # data from mobile must be parsed
    if data == '' or data is None:
        email = request.form.get('email')
        password = request.form.get('password')
    else:
        parsed_data = json.loads(data)
        email = parsed_data["email"]
        password = parsed_data["password"]

    conn = sqlite3.connect('csgo_strats.db')
    c = conn.cursor()
    c.execute("SELECT email,name,password,id FROM users where email = ? ;", (email,))
    result_email_check = c.fetchall()

    for res in result_email_check:
        if verify_password(res[2], password):
            token = randomString() + ';'+str(res[3])
            now = datetime.date.today()
            formated_time = now + datetime.timedelta(days=1)
            c.execute("UPDATE users set token = ? , valid_to = ? WHERE id = ?",(token, str(formated_time), int(res[3]),))
            conn.commit()
            response['status'] = 200
            response['message'] = 'Login Successful'
            response['data']['name'] = res[1]
            response['data']['email'] = res[0]
            response['data']['token'] = token
        else:
            response['message'] = 'Login Failed'

    conn.close()

    return json.dumps(response)


#app.run(host='10.0.0.76', port=5000)
@app.route('/teste/', methods=['GET'])
def tseeste4():
    return json.dumps("response")


if __name__ == '__main__':
    app.run()



